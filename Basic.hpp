// Basic data structures needed to represent coordinate system

#include <assert.h>

class Tuple {

protected:  
  float x;
  float y;
  float z;
  float w;
  
public:
  Tuple (){
      w = 1.0f;
      this->x = 0.0f;
      this->y = 0.0f;
      this->z = 0.0f;
    }
  Tuple (float x, float y, float z, float w){
      this->w = w;
      this->x = x;
      this->y = y;
      this->z = w;
  }

  Tuple (const Tuple &b){
      w = b.w;
      x = b.x;
      y = b.y;
      z = b.w;
  }

  void print (){
    std::cout << x <<" "<< y <<" "<< z <<" "<< w;
  }

  // operator mVector() const { return ; }

// compare perf b/w the 2 impl
// add mPoint-vector :return: mPoint after moving in dir of vector from org mPoint
// add vector-vector :return: result vector addition
  Tuple add (Tuple b){
    assert(("!ERR : Invalid Op::trying to add 2 mPoints!",!(w == 0 && b.get(3) == 0)));
    return Tuple(x + b.get(0), y + b.get(1), z + b.get(2), w + b.get(3));
  }
  // Tuple add (Tuple b){
  //   if (w == 0 && b.get(3) == 0){
  //     cout <<"ERR : Invalid Op::trying to add 2 mPoints";
  //     return;
  //   }
  // 
  //   x += b.get(0);
  //   y += b.get(1);
  //   z += b.get(2);
  //   w += b.get(3);
  // }

// 0 : for x , 1 : for y
// 2 : for z , 3 : for w
  float get(int coord) const{
    switch (coord)
    {
    case 0:
      return x;
      break;
    case 1:
      return y;
      break;
    case 2:
      return z;
      break;  
    case 3:
      return w;
      break;

    default:
      break;
    }
  }
};

class mPoint : public Tuple{

public :
  mPoint (){
      w = 1.0f;
      this->x = 0.0f;
      this->y = 0.0f;
      this->z = 0.0f;
    }

  mPoint (float x, float y, float z){
    w = 1.0f;
    this->x = x;
    this->y = y;
    this->z = z;
  }
  mPoint (const Tuple &b){
    assert(("!ERR : Invalid Op::incompatible Tuple for mPoint!",b.get(3) == 1));
    x = b.get(0);
    y = b.get(1);
    z = b.get(2);
    w = b.get(3);
  }

};

class mVector : public Tuple{
public:
  mVector (){
    w = 0.0f;
    this->x = 0.0f;
    this->y = 0.0f;
    this->z = 0.0f;
  }

  mVector (float x, float y, float z){
    w = 0.0f;
    this->x = x;
    this->y = y;
    this->z = z;
  }
  mVector (const Tuple &b){
    assert(("!ERR : Invalid Op::incompatible Tuple for mVector!",b.get(3) == 1));
    x = b.get(0);
    y = b.get(1);
    z = b.get(2);
    w = b.get(3);
  }
};